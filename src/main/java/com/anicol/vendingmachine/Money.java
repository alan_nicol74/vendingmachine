/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anicol.vendingmachine;

/**
 *
 * @author Alan.Nicol
 */
public enum Money {

    TWO_POUND(2.00, false), ONE_POUND(1.00, true), FIFTY_PENCE(0.50, true), TWENTY_PENCE(0.20, true),
    TEN_PENCE(0.10, true), FIVE_PENCE(0.05, false), TWO_PENCE(0.02, false),ONE_PENCE(0.01, false);

    private final double value;
    private final boolean valid;

    Money(double value, boolean valid) {
        this.value = value;
        this.valid = valid;
    }

    public double getValue() {
        return value;
    }

    public boolean isValid() {
        return valid;
    }

}
