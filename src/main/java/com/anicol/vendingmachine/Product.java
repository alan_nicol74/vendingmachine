/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anicol.vendingmachine;

/**
 *
 * @author Alan.Nicol
 */
public class Product {

    private ProductType productType;
    private Selector selector;

    public Product(ProductType productType, Selector selector) {
        this.productType = productType;
        this.selector = selector;
    }

    public ProductType getProductType() {
        return productType;
    }

    public void setProductType(ProductType productType) {
        this.productType = productType;
    }

    public Selector getSelector() {
        return selector;
    }

    public void setSelector(Selector selector) {
        this.selector = selector;
    }

}
