/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.anicol.vendingmachine;

/**
 *
 * @author Alan.Nicol
 */
public enum ProductType {

    CHOCOLATE_BAR("Chocolate Bar",0.60), FIZZY_DRINK("Chocolate Bar",1.00), 
    SANDWICH("Chocolate Bar",1.70);

    private final String name;
    private final double cost;

    private ProductType(String name, double cost) {
        this.name = name;
        this.cost = cost;
    }

    public String getName() {
        return name;
    }

    public double getCost() {
        return cost;
    }
}
