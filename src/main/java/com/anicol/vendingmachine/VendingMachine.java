package com.anicol.vendingmachine;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

/**
 * Encapsulates the state of a vending machine and the operations that can be
 * performed on it
 */
public class VendingMachine {

    private boolean running;
    private double insertedMoney = 0.0;
    private double changeValue = 0.0;
    private final List<Money> change;
    private final List<Product> products;

    public VendingMachine() {
        super();

        products = stockProducts();
        change = stockChange();
    }

    public Money insertMoney(Money money) {
        if ((running) && (money.isValid())) {
            addMoney(money);
            return null;
        } else {
            return money;
        }

    }

    public Product select(Selector selector) {
        ProductType productType;

        if (running) {
            productType = getProductType(selector);

            if ((productStocked(productType)) && enoughMoney(productType)) {
                return purchaseProduct(productType);
            }
        }

        return null;
    }

    public List<Money> returnCoin() {
        List<Money> money = null;

        if ((running) && (changeValue > 0.0)) {
            money = calculateReturnedCoins(changeValue);
        }

        return money;
    }

    public void setOn() {
        running = true;
    }

    public void setOff() {
        running = false;
    }

    public boolean isOn() {
        return running;
    }

    private List<Product> stockProducts() {
        List<Product> productList = new ArrayList();

        productList = addProduct(productList, ProductType.CHOCOLATE_BAR, Selector.A);
        productList = addProduct(productList, ProductType.FIZZY_DRINK, Selector.B);
        productList = addProduct(productList, ProductType.SANDWICH, Selector.C);

        return productList;
    }

    private List<Product> addProduct(List<Product> productList, ProductType productType, Selector selector) {

        for (int x = 0; x < 10; x++) {
            productList.add(createProduct(productType, selector));
        }

        return productList;
    }

    private Product createProduct(ProductType productType, Selector selector) {
        return new Product(productType, selector);
    }

    private List<Money> stockChange() {
        List<Money> changeList = new ArrayList();

        changeList = addMoney(changeList, Money.TEN_PENCE);
        changeList = addMoney(changeList, Money.TWENTY_PENCE);
        changeList = addMoney(changeList, Money.FIFTY_PENCE);
        changeList = addMoney(changeList, Money.ONE_POUND);

        return changeList;
    }

    private void addMoney(Money money) {
        insertedMoney += money.getValue();
        change.add(money);
    }

    private ProductType getProductType(Selector selector) {

        switch (selector) {
            case A:
                return ProductType.CHOCOLATE_BAR;
            case B:
                return ProductType.FIZZY_DRINK;
            case C:
                return ProductType.SANDWICH;
        }

        return null;
    }

    private boolean productStocked(ProductType productType) {

        return findProduct(productType) != null;
    }

    private boolean enoughMoney(ProductType productType) {

        return insertedMoney >= productType.getCost();
    }

    private Product purchaseProduct(ProductType productType) {
        Product product;

        product = findProduct(productType);
        products.remove(product);
        changeValue = calculateChangeValue(productType.getCost());

        return product;
    }

    private Product findProduct(ProductType productType) {
        for (Product product : products) {
            if (product.getProductType() == productType) {
                return product;
            }
        }

        return null;
    }

    private double calculateChangeValue(double cost) {

        return round(insertedMoney - cost, 2);
    }

    public static double round(double value, int places) {
        if (places < 0) {
            throw new IllegalArgumentException();
        }

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    private List<Money> calculateReturnedCoins(double remainingChange) {
        List<Money> returnedCoins;
        double currentRemainingChange;

        returnedCoins = new ArrayList();
        for (Money money : Money.values()) {
            currentRemainingChange = remainingChange;
            remainingChange = calculateRemainingChange(remainingChange, money);
            if (currentRemainingChange > remainingChange) {
                returnedCoins.add(money);
                removeChangeFromChangeList(money.getValue());
                if (remainingChange == 0.0) {
                    break;
                }
            }

        }

        return returnedCoins;
    }

    private double calculateRemainingChange(double value, Money money) {
        if (value >= money.getValue()) {
            value = round(value % money.getValue(),2);
        }

        return value;
    }

    private void removeChangeFromChangeList(double value) {

        for (Money money : change) {
            if (money.getValue() == value) {
                change.remove(money);
                break;
            }
        }
    }

    private List<Money> addMoney(List<Money> changeList, Money money) {

        for (int x = 0; x < 10; x++) {
            changeList.add(money);
        }

        return changeList;
    }

    public double getInsertedMoney() {
        return insertedMoney;
    }

    public List<Money> getChange() {
        return change;
    }

    public List<Product> getProducts() {
        return products;
    }

}
