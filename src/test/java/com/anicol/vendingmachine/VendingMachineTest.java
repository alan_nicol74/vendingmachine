package com.anicol.vendingmachine;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

/**
 * Unit tests for {@link VendingMachine}
 */
public class VendingMachineTest {

    @Test
    public void defaultStateIsOff() {
        VendingMachine machine = new VendingMachine();
        assertFalse(machine.isOn());
    }

    @Test
    public void turnsOn() {
        VendingMachine machine = new VendingMachine();
        machine.setOn();
        assertTrue(machine.isOn());
    }
    
    @Test
    public void turnsOff() {
        VendingMachine machine = new VendingMachine();
        machine.setOn();
        machine.setOff();
        assertFalse(machine.isOn());
    }
    
    @Test
    public void containsProducts() {
        VendingMachine machine = new VendingMachine();
        assertTrue(machine.getProducts().size() > 0);
    }
    
    @Test
    public void containsChange() {
        VendingMachine machine = new VendingMachine();
        assertTrue(machine.getChange().size() > 0);
    }
    
    @Test
    public void containsNoInsertedMoney() {
        VendingMachine machine = new VendingMachine();
        assertFalse(machine.getInsertedMoney() > 0.0);
    }
    
    @Test
    public void selectItemWithNoInsertedMoney() {
        VendingMachine machine = new VendingMachine();
        machine.setOn();
        assertTrue(machine.select(Selector.A) == null);
    }
    
    @Test
    public void returnCoinWithNoInsertedMoney() {
        VendingMachine machine = new VendingMachine();
        machine.setOn();
        assertTrue(machine.returnCoin() == null);
    }
    
    @Test
    public void insertMoneyWhenSwitchedOff() {
        VendingMachine machine = new VendingMachine();
        assertTrue(machine.insertMoney(Money.ONE_POUND)!= null);
    }
    
    @Test
    public void selectItemWhenSwitchedOff() {
        VendingMachine machine = new VendingMachine();
        
        machine.insertMoney(Money.ONE_POUND);
        assertTrue(machine.select(Selector.A) == null);
    }
    
    @Test
    public void returnCoinWhenSwitchedOff() {
        VendingMachine machine = new VendingMachine();
        
        machine.setOn();
        machine.insertMoney(Money.ONE_POUND);
        machine.select(Selector.A);
        machine.setOff();
        assertTrue(machine.returnCoin() == null);
    }
    
    @Test
    public void selectItemWithIncorrectChange() {
        VendingMachine machine = new VendingMachine();
        
        machine.setOn();
        machine.insertMoney(Money.FIFTY_PENCE);
        assertTrue(machine.select(Selector.B) == null);
    }
    
    @Test
    public void selectItemWithCorrectChange() {
        VendingMachine machine = new VendingMachine();
        machine.setOn();
        machine.insertMoney(Money.ONE_POUND);
        machine.insertMoney(Money.FIFTY_PENCE);
        machine.insertMoney(Money.TWENTY_PENCE);
        assertTrue(machine.select(Selector.C) != null);
    }
    
    @Test
    public void selectItemWithTooMuchChange1() {
        VendingMachine machine = new VendingMachine();
        
        machine.setOn();
        machine.insertMoney(Money.ONE_POUND);
        machine.insertMoney(Money.FIFTY_PENCE);
        machine.insertMoney(Money.FIFTY_PENCE);
        assertTrue(machine.select(Selector.C) != null);
        assertTrue(machine.returnCoin().size() > 0);
    }
    
    @Test
    public void selectItemWithTooMuchChange2() {
        VendingMachine machine = new VendingMachine();
        
        machine.setOn();
        machine.insertMoney(Money.TEN_PENCE);
        machine.insertMoney(Money.ONE_POUND);
        machine.insertMoney(Money.FIFTY_PENCE);
        assertTrue(machine.select(Selector.A) != null);
        assertTrue(machine.returnCoin().size() > 0);
    }
    
    @Test
    public void selectItemWithTooMuchChange3() {
        VendingMachine machine = new VendingMachine();
        
        machine.setOn();
        machine.insertMoney(Money.FIFTY_PENCE);
        machine.insertMoney(Money.TWENTY_PENCE);
        machine.insertMoney(Money.TEN_PENCE);
        machine.insertMoney(Money.TWENTY_PENCE);
        machine.insertMoney(Money.TEN_PENCE);
        assertTrue(machine.select(Selector.A) != null);
        assertTrue(machine.returnCoin().size() > 0);
    }
    
    @Test
    public void selectItemWithTooMuchChange4() {
        VendingMachine machine = new VendingMachine();
        
        machine.setOn();
        machine.insertMoney(Money.TWENTY_PENCE);
        machine.insertMoney(Money.TWENTY_PENCE);
        machine.insertMoney(Money.TEN_PENCE);
        machine.insertMoney(Money.TWENTY_PENCE);
        machine.insertMoney(Money.TEN_PENCE);
        machine.insertMoney(Money.FIFTY_PENCE);
        assertTrue(machine.select(Selector.B) != null);
        assertTrue(machine.returnCoin().size() > 0);
    }
    
    @Test
    public void selectItemWithTooMuchChange5() {
        VendingMachine machine = new VendingMachine();
        
        machine.setOn();
        machine.insertMoney(Money.TEN_PENCE);
        machine.insertMoney(Money.TEN_PENCE);
        machine.insertMoney(Money.TEN_PENCE);
        machine.insertMoney(Money.TEN_PENCE);
        machine.insertMoney(Money.TEN_PENCE);
        machine.insertMoney(Money.TWENTY_PENCE);
        machine.insertMoney(Money.TWENTY_PENCE);
        machine.insertMoney(Money.TWENTY_PENCE);
        machine.insertMoney(Money.FIFTY_PENCE);
        machine.insertMoney(Money.FIFTY_PENCE);
        assertTrue(machine.select(Selector.C) != null);
        assertTrue(machine.returnCoin().size() > 0);
    }
}
